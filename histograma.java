package histograma;
//1)array 2)arraylist 3)collection.sort
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Histograma {

    public static Map<Integer, Integer> histogram(int[] array){
        
        Map<Integer, Integer> map = new HashMap<>();
        int keyValue;
        
        for (int i = 0; i < array.length; i++) {
            
            if (map.containsKey(array[i])){
                
                keyValue = map.get(array[i]);
                keyValue++;
                map.put(array[i], keyValue);
                
            }else map.put(array[i],1);
        }
        return map;
    }
    
    
    public static Map<Integer, Integer> mostKnownValue(Map<Integer, Integer> map){
    
        int maxValue = 0;
        int value = 0;
        Iterator it = map.entrySet().iterator();
        
        while (it.hasNext()){
            
            Map.Entry pairs = (Map.Entry) it.next();
            
            if((int)pairs.getValue() > maxValue){
                
                maxValue = (int) pairs.getValue();
                value = (int) pairs.getKey();
            }
            
            it.remove();
        }
        
        Map<Integer, Integer> result = new HashMap<>();
        result.put(value, maxValue);
        
        return result;
    }
    
    
    public static void printMap(Map<Integer, Integer> map){
        
        Iterator it;
        it = map.entrySet().iterator();
        
        while (it.hasNext()) {
            Map.Entry pairs = (Map.Entry) it.next();
            System.out.println("Key = " +pairs.getKey()+ 
                               " => Value = " + pairs.getValue());
            it.remove();
        }
    }
    
    
    public static void main(String[] args) {
        
        final int range = 20;
        
        int[] array = new int[range];
        for (int i = 0; i < range; i++) {
            array[i] = i+1;
        }
        
        int[] array2 = {5,5,5,5,5,20,85};
        
        Map<Integer, Integer> map = histogram(array);       
        Map<Integer, Integer> map2 = histogram(array2);
        
        System.out.println("1st test");
        printMap(map);
        System.out.println("-------------------------------");
        printMap(mostKnownValue(map));
        
        System.out.println("2nd test");
        printMap(map2);
        System.out.println("-------------------------------");
        printMap(mostKnownValue(map2));        
  
    }
}
